var $ = jQuery.noConflict();
$(document).ready(function () {

	/*Consumo da api para o menu*/
	$.getJSON('/api/V1/categories/list', function (data) {
		var html = `<li class="nav-item"><a class="nav-link" href="/">Pagina Inicial</a></li>
					<li class="nav-item" id="${data.items[0].id}" data-id="${data.items[0].id}"><a class="nav-link">${data.items[0].name}</a></li>
					<li class="nav-item" id="${data.items[1].id}" data-id="${data.items[1].id}"><a class="nav-link">${data.items[1].name}</a></li>
					<li class="nav-item" id="${data.items[2].id}" data-id="${data.items[2].id}"><a class="nav-link">${data.items[2].name}</a></li>`;
		$('ul#menu').html(html);
	});

	/*------------------filter for categories-------------------------*/


	const
		// RESPOSTA DO SERVER
		getDataFromServer = function (id, callback) {
			// return categoriesResponse;
			const
				url = `/api/V1/categories/${id}`;

			return $.getJSON(url, function (response) {
				console.log('response is: ', response);
				callback(response);
			});
		},

		// FUNÇÃO PARA MOSTRAR OS ITENS
		showItems = function (items) {
			let htmlData = [],
				html = '';
			for (var i = 0; i < items.length; i++) {
				htmlData.push(`
	            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-8 mobile">
	   				<div class="card" id="${items[i].id}">
	            		<img class="card-img-top" src="${items[i].image}" alt=" "/>
	            		<div class="card-body">
	                		<h5 class="card-title">${items[i].name}</h5>
	                 		<p class="card-text">R$ ${items[i].price}</p>
	                 		<a href="#" class="btn btn-primary">COMPRAR</a>
	             		</div>
	           		</div>
	         	</div>`
				);
			}
			html = htmlData.join(' ');
			return html;
		},
		// FAZ O FILTRO PARA EXIBIR OS PRODUTOS PELA COR
		showListItemsByTypeValue = function (type, valor, list) {
			// showListItemsByTypeValue('color', 'preto') // // showListItemsByTypeValue('gender', 'feminino')
			const
				items = [];
			list.items.map(function (item) {
				for (let index = 0; index <= item.filter.length - 1; index++) {
					const
						currentFilter = item.filter[index];
					if (currentFilter[type] === valor) {
						items.push(item);
					}
				}
			});
			console.table('os items para essa cor são: ', items);
			$('.products').html(showItems(items));
		};
	// Quando a pessoa clica nas categorias (roupas, sapatos)
	$(document).on("click", ".nav-item", function (evt) {	
		const
			$element = $(this),
			// pego a cor que este item possui
			id = $element.data('id');
		evt.preventDefault();

		getDataFromServer(id, function (responseFromServer) {
			$('.products').html(showItems(responseFromServer.items));
			localStorage.setItem("responseFromServer", JSON.stringify(responseFromServer));
		});

	});
	
	 //A pessoa clicou em alguma cor
	$('.color-item').on('click', function (evt) {
		const
			$element = $(this),
			// pego a cor que este item possui
			color = $element.data('color');

		// chamo a função q vai ser responsável por mostrar/filtrar os itens que possuem esta cor
		showListItemsByTypeValue('color', color, JSON.parse(localStorage.getItem('responseFromServer')));
	});
	 //A pessoa clicou em alguma gênero
	$('.categories a').on('click', function (evt) {
		const
			$element = $(this),
			
			gender = $element.data('gender');
		evt.preventDefault();
		showListItemsByTypeValue('color', color);
	});



});      
